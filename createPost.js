#!/usr/bin/env node
var site = process.argv[2];
var name = process.argv[3];

createPost({
  title: name,
  slug: name,
  website: site,
  image: name + '.png'
});

// var YAML = require('yamljs');

function createPost(opts) {

  var YAML = require('node-yaml');

  var defaults = {
    layout: 'post',
    title: '',
    slug: '',
    website: '',
    image: '',
    categories: []
  };

  var options = Object.assign(defaults, opts);

  YAML.write('_posts/' + name + '.md', options);

}
